# "Aggregation of Multi-Armed Bandits learning algorithms for Opportunistic Spectrum Access in Cognitive Radio"
This repository contains the LaTeX code of a research article written by [Lilian Besson](http://perso.crans.org/besson/) and collaborators ([Emilie Kaufmann](http://chercheurs.lille.inria.fr/ekaufman/research.html) and [Christophe Moy](http://www.rennes.supelec.fr/ren/perso/cmoy/recherche.php)), entitled "Aggregation of Multi-Armed Bandits learning algorithms for Opportunistic Spectrum Access in Cognitive Radio".

Our article ([see this PDF](https://www.overleaf.com/docs/10752148zfbfryxsnsmd/pdf.pdf)) was accepted to the [IEEE WCNC 2018](http://wcnc2018.ieee-wcnc.org/authors/call-papers) conference in December 2017.
I will present it in Barcelona, Spain, in April 2018.

- PDF : [BKM_IEEEWCNC_2018.pdf](https://hal.inria.fr/hal-01705292/document)
- HAL notice : [BKM_IEEEWCNC_2018](https://hal.inria.fr/hal-01705292/)
- BibTeX : [BKM_IEEEWCNC_2018.bib](https://hal.inria.fr/hal-01705292/bibtex)
- Source code and documentation: [http://banditslilian.gforge.inria.fr/Aggregation.html](http://banditslilian.gforge.inria.fr/Aggregation.html)

[![Published](https://img.shields.io/badge/Published%3F-accepted-green.svg)](https://hal.inria.fr/hal-01705292)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-finished-green.svg)](https://bitbucket.org/lbesson/aggregation-of-multi-armed-bandits-learning-algorithms-for/commits/)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://bitbucket.org/lbesson/ama)

> [I (Lilian Besson)](http://perso.crans.org/besson/) have [started my PhD](http://perso.crans.org/besson/phd/) in October 2016, and this is a part of my **on going** research in 2017.

----

## :scroll: License ? [![GitHub license](https://img.shields.io/github/license/Naereen/badges.svg)](https://bitbucket.org/lbesson/aggregation-of-multi-armed-bandits-learning-algorithms-for/src/master/LICENSE)
[MIT Licensed](https://lbesson.mit-license.org/) (file [LICENSE](LICENSE)).

© 2017 [Lilian Besson](http://perso.crans.org/besson/) and collaborators.

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://bitbucket.org/lbesson/aggregation-of-multi-armed-bandits-learning-algorithms-for/commits/)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://bitbucket.org/lbesson/ama)
[![Analytics](https://ga-beacon.appspot.com/UA-38514290-17/bitbucket.org/lbesson/aggregation-of-multi-armed-bandits-learning-algorithms-for/README.md?pixel)](https://bitbucket.org/lbesson/aggregation-of-multi-armed-bandits-learning-algorithms-for/)
[![ForTheBadge uses-badges](http://ForTheBadge.com/images/badges/uses-badges.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-git](http://ForTheBadge.com/images/badges/uses-git.svg)](https://GitHub.com/)

[![forthebadge made-with-overleaf](https://img.shields.io/badge/Made%20with-OverLeaf-1f425f.svg)](https://www.overleaf.com/)
[![forthebadge made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](http://perso.crans.org/besson/)
